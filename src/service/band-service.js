import axios from 'axios';
/**
 * L'idée de cette classe est d'avoir un endroit dédié aux appels XHR
 * vers le serveur et donc de regrouper toutes les transformation de 
 * réponse ou requête serveur dans un même fichier.
 * Cela nous permettra dans le reste du code d'avoir directement des
 * objets utilisables bien comme il faut.
 * C'est le même principe que lorsqu'on faisait des DAO/Repository avec
 * PDO en Symfony
 */
export class BandService {
    constructor() {
        /**
         * Url du Webservice Rest sur lequel on tape
         */
        this.url = 'http://localhost:8000/api/band';
    }
    
    async findAll() {
        let response = await axios.get(this.url);
        return response.data;
    }

    add(band) {
        return axios.post(this.url, band)
        .then(response => response.data);

        //on peut ecrire strictement la même chose qu'au dessus comme ça
        // return axios.post(this.url, band)
        // .then(function(response) {
        //     return response.data;
        // });

        //ou alors comme ça à l'intérieur d'une fonction async
        // const response = await axios.post(this.url, band);
        // return response.data;
    }

    async remove(id) {
        let response = await axios.delete(this.url+'/'+id);
        return response.status === 204;
    }
}