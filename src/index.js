import $ from 'jquery';
import { BandService } from './service/band-service';

const bandService = new BandService();

bandService.findAll().then(bandList => {
    for (const band of bandList) {
        displayBand(band);
    }

});

$('#add-form').on('submit', function(event) {
    event.preventDefault();
    let formValue = {
        name: $('#name').val(),
        start: $('#start').val(),
        end: $('#end').val(),
        country: $('#country').val()
    };
    bandService.add(formValue).then(band => {
        $('#error').empty();
        displayBand(band);
    })
    .catch(() => $('#error').text('Server Problem'));
    

});



function displayBand(band) {

    let li = $('<li></li>')
        .text(`Band name : ${band.name}, Career Start : ${band.start}, Carrer End : ${band.end}, Country : ${band.country}`)
        .appendTo('#bands');
    $('<button>remove</button>')
    .on('click', function() {
        bandService.remove(band.id).then((success) => {
            if(success) {
                li.css({left: 200, opacity:0}).on('transitionend', () => li.remove());
            }
        })
    })
    .appendTo(li);

}
// $('#bands').append('<li>bloup</li>');
// $('<li></li>')
// .text('blip')
// .appendTo('#bands');



// document.querySelectorAll('p').forEach(el => el.style.color = 'red');

// $('#bands').on('', () => {});

// let p = $('<p></p>').css('color', 'red')
// .addClass('maClasse')
// .append('<span>test</span>')
// .on('click', () => alert('bloup'));

// p.appendTo($('main'));