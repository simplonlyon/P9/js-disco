# js-disco
Un projet Webpack à utiliser avec le projet symfony-disco pour voir les interactions client/serveur
## How To Use
1. Cloner le projet
2. Faire un `npm install`
3. Faire un `npm run dev` pour lancer le webpack
4. Aller sur http://localhost/js-disco pour voir le projet (ne pas oublier de lancer le serveur)

## Exercice

### I. API Rest en Symfony
1. Créer un projet symfony avec composer (website skeleton)
2. Faire un require de cors
3. Créer une entité Band qui aura comme propriété : name (string), start (DateTime), end (?DateTime), country (string)
4. Créer un Form pour cet entité
5. Faire un require de orm-fixtures et créer une fixture pour créer 4-5 bands
6. Faire le contrôleur Rest pour avec les 5 méthodes  ("inspirez" vous de symfony-rest pour faire pareil)
7. Tester ses routes, soit avec le RestClient, soit avec des tests fonctionnels
### II. Projet Front
1. Créer un projet javascript avec npm js-disco, et mettre et configurer webpack dedans comme il faut
2. Installer axios et jquery avec npm
3. Créer un dossier src/service et dedans mettre un fichier band-service.js
4. Dans ce fichier, créer une class javascript qui aura une propriété url avec comme valeur 'http://localhost:8000/api/band'
5. Faire les méthodes correspondantes à celle du contrôleur Rest dans cette classe en async, qui feront les requêtes avec axios
### III. Afficher les groupes
1. Utiliser la méthode findAll du BandService pour récupérer les groupes
2. Dans le then du findAll, faire une boucle puis utiliser jQuery pour créer les balises html de chaque groupe et les append dans un ul
### IV. Ajouter un groupe
1. Dans la classe BandService, faire une nouvelle méthode (sans utiliser la syntaxe async) add(band)
2. Dans cette méthode, utiliser axios.post pour faire un post vers le serveur en lui donnant à manger l'argument band (qui du coup devra contenir un nouveau groupe)
En gros dans cette méthode il va falloir faire un return du axios.post pour renvoyer la Promise, puis faire un .then() sur le axios.post dans lequel on fera un return pour changer la valeur de la promise
3. Faire ensuite un formulaire dans le index.html qui permettra de rajouter un nouveau groupe (donc 4 inputs)
4. Dans le index.js sélectionner le formulaire et rajouter un event au submit de celui ci (faire le event.preventDefault() dedans)
5. Récupérer les valeurs des inputs d'une manière ou d'une autre dans l'event et en faire un objet JS et le donner à manger à la méthode add de l'instance de BandService
6. Dans le then de cette méthode, faire en sorte de rajouter le nouveau groupe dans la liste des groupes affichés (ou afficher une erreur dans le catch si ça a pas marché)
### V. La suppression
1. Faire la méthode delete côté symfony si elle est pas déjà faites (s'inspirer du projet symfony-rest comme d'hab)
2. Dans le BandService, créer une nouvelle méthode remove(band) qui fera un axios.delete vers l'url qu'il faut
il faudra concaténer l'id de l'argument band à l'url de la classe (séparés par un "/")
3. Dans le index.js, modifier la partie affichage pour faire en sorte de rajouter un bouton remove
4. Sur ce bouton remove, rajouter un event au click qui déclenchera la méthode remove du service
5. Dans le then du remove, faire en sorte de dégager l'élément correspondant de l'affichage


## Cahier 2 Vacances
### La sélection d'un groupe
Faire en sorte que lorsque l'on clique sur un des li
des bands, cela mette celui ci dans une variable.

Ensuite, faire une fonction qui affiche d'une manière
ou d'une autre le groupe sélectionné (soit dans une
fenêtre modale, soit sur le côté de la liste, soit 
tout en haut...)
### La mise à jour d'un groupe
Côté symfony, créer la route Patch pour pouvoir mettre à jour un groupe sur le serveur (s'inspirer du projet symfony-rest).

Côté JS, dans le BandService, créer la méthode update(band) qui tapera sur cette route.

Dans le index.js faire que lorsqu'on sélectionne un groupe, ça nous affiche ses informations sous forme de formulaire pré-rempli.
Lorsque l'on valide ce formulaire, cela lancera la méthode update du BandService, puis dans son then, ça mettra à jour l'affichage de la liste des groupes pour qu'elle corresponde à la mise à jour.

Correction sur la branche solution d'ici la fin de la semaine
### Des belles dates
Faire un npm install de moment, puis, côté BandService, modifier le then de toutes les méthodes qui renvoie un/des band(s) (et donc trouver un moyen de refactoriser ça ?) et faire en sorte de changer la valeur de start par un objet date moment js. Pareil pour end mais en vérifiant s'il est null ou pas avant.

Modifier la fonction d'affichage pour faire en sorte de faire un format() sur la date pour l'afficher au format jj/mm/aaaa

Il faudra aussi faire des modifications sur la partie mise à jour pour que ça continue à marcher